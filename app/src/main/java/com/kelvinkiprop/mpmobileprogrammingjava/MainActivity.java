package com.kelvinkiprop.mpmobileprogrammingjava;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

//-----------------------------------MainActivity extends AppCompatActivity-------------------------------//
public class MainActivity extends AppCompatActivity {

    //Declare
    private Button btnLogin, btnExit;
    private EditText editTextName;

    //-----------------------------------onCreate-------------------------------//
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Change title
        this.setTitle("Main Activity");

        //Initialization
        editTextName = findViewById(R.id.editTextName);
        btnLogin = findViewById(R.id.buttonLogin);
        btnExit = findViewById(R.id.buttonExit);

        //Onclick
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Get value
                String name = editTextName.getText().toString();

                if (name.isEmpty()){
                    //Show
                    Toast.makeText(MainActivity.this, "Input field is empty", Toast.LENGTH_SHORT).show();

                }else {
//                    Toast.makeText(MainActivity.this, ""+name, Toast.LENGTH_SHORT).show();

                    //To second activity
                    //Method 1
//                    Intent intent = new Intent(MainActivity.this, SecondActivity.class);
//                    intent.putExtra("message", name);
//                    startActivity(intent);
//                    finish();

                    //Method 2
                    Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("name", name);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                }

            }
        });

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle("Attention!");
                alertDialog.setMessage("Do you want to exit?");

                //Positive button
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                //Negative button
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                System.exit(0);
                            }
                        });

                alertDialog.show();

            }
        });

    }
    //-----------------------------------./onCreate-------------------------------//

}
//-----------------------------------.MainActivity extends AppCompatActivity-------------------------------//