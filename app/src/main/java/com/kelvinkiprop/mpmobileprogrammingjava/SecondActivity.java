package com.kelvinkiprop.mpmobileprogrammingjava;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

//-----------------------------------SecondActivity extends AppCompatActivity-------------------------------//
public class SecondActivity extends AppCompatActivity {

    //Variables declaration
    private TextView tvShowMessageFromMainActivity;
    private Button btnToGoogle, btnSaveDetails;
    private EditText etEmail, etAge;
    private RadioGroup rgGender;
    private RadioButton rbGender;

    //-----------------------------------onCreate-------------------------------//
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        //Set
        this.setTitle("Second Activity");

        //Initialize variables
        tvShowMessageFromMainActivity = findViewById(R.id.textViewText);
        etEmail = findViewById(R.id.editTextEmail);
        etAge = findViewById(R.id.editTextAge);

        rgGender = findViewById(R.id.radioGroupGender);

        btnToGoogle = findViewById(R.id.buttonToGoogle);
        btnSaveDetails = findViewById(R.id.buttonSaveDetails);

        //Get intent data
//        // method 1
//        String nameFromMainActivity = getIntent().getStringExtra("message");
//        tvShowMessageFromMainActivity.setText(nameFromMainActivity);
        // method 2
        Bundle bundle = getIntent().getExtras();
        String nameFromMainActivity = bundle.getString("name");
        tvShowMessageFromMainActivity.setText("Bienvenue "+nameFromMainActivity);

        //OnClick
        btnToGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Implicit intent
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com/"));
                startActivity(intent);
                finish();
            }
        });

        btnSaveDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Get values
                String email = etEmail.getText().toString();
                String age = etAge.getText().toString();
                rbGender = findViewById(rgGender.getCheckedRadioButtonId());
                String gender = rbGender.getText().toString();

                Toast.makeText(getApplicationContext(), "Entered details \n Email:"+email+"\n Age: "+age+"\n Gender: "+gender, Toast.LENGTH_SHORT).show();
            }
        });

    }
    //-----------------------------------./onCreate-------------------------------//

}
//-----------------------------------./SecondActivity extends AppCompatActivity-------------------------------//